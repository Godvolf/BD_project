package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Gui extends JFrame {
  Databaseconnection dbc= new Databaseconnection();
  int count=0;
  JDialog loginwindow;
  Boolean returned;
  JLabel a,b, worker,admin;
  JTextField login,password;
  JButton OK, workbutt,adminbutt;
  String log,pas, workaction, adminaction, NIPnumber, name, table, where;
  JComboBox workerbox, adminbox;
  Gui(){
    String tabele[] = {"Klienci","Dokumenty","Segregatory","Kategorie_Dokumentow"};
    JComboBox tablebox= new JComboBox(tabele);


    setTitle("Document storage");
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setBounds(300,100,700,400);
    setLayout(new GridLayout(3,4));

    //login window
    loginwindow= new JDialog();
    loginwindow.setTitle("Login window");
    loginwindow.setDefaultCloseOperation(HIDE_ON_CLOSE);
    loginwindow.setVisible(true);
    loginwindow.setLayout(new GridLayout(5,1));
    loginwindow.setBounds(500,300,500,350);

    a=new JLabel("Wpisz login");
    login=new JTextField(20);
    b=new JLabel("Wpisz haslo");
    password=new JTextField(20);
    OK = new JButton("OK");

    loginwindow.add(a);
    loginwindow.add(login);
    loginwindow.add(b);
    loginwindow.add(password);
    loginwindow.add(OK);

    OK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        log = login.getText();
        pas = password.getText();
        loginwindow.setVisible(false);
        returned = dbc.connect(log, pas);
        if (returned == false) {
          JDialog error = new JDialog();
          error.setVisible(true);
          error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
          error.setTitle("Bledne dane. Sprawdz dane i zaloguj sie ponownie");
          JButton confirm = new JButton("OK");
          error.setBounds(500, 300, 300, 150);
          error.add(confirm);
          confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              error.setVisible(false);
              loginwindow.setVisible(true);
            }
          });
        }
        else {
          setVisible(true);
        }
      }});

    //Main frame stuff
    JLabel stazysta= new JLabel("Stazysta: ");
    worker= new JLabel("Pracownik:");
    admin= new JLabel("Admin");
    JLabel root= new JLabel("root");
    String comboBoxItems2[] = {"Wyszukaj rekord"};
    String comboBoxItems[] = {"Wprowadz dokument", "Wprowadz klienta", "Wprowadz kategorie dokumentu", "Wprowadz segregator"};
    String comboBoxItems1[] = {"Usun rekord"};
    String comboBoxItems3[]= {"dodaj uzytkownika"};
    JComboBox stazystabox = new JComboBox(comboBoxItems2);
    workerbox= new JComboBox(comboBoxItems);
    adminbox= new JComboBox(comboBoxItems1);
    JComboBox rootbox= new JComboBox(comboBoxItems3);
    JButton stazystabutt= new JButton("Send");
    workbutt=new JButton("Send");
    adminbutt=new JButton("Send");
    JButton rootbutt= new JButton("Send");

    add(stazysta);
    add(worker);
    add(admin);
    add(root);
        add(stazystabox);
    add(workerbox);
    add(adminbox);
    add(rootbox);
    add(stazystabutt);
    add(workbutt);
    add(adminbutt);
    add(rootbutt);
    stazystabutt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        //wyszukiwanie rekordu
          setVisible(false);
          JDialog selector= new JDialog();
          selector.setVisible(true);
          selector.setTitle("Wyszukaj element");
          selector.setDefaultCloseOperation(HIDE_ON_CLOSE);
          selector.setLayout(new GridLayout(5,1));
          selector.setBounds(500,300,500,350);
          JLabel tab= new JLabel("Z ktorej tabeli:");
          selector.add(tab);
          selector.add(tablebox);
          JLabel warunek= new JLabel("Gdzie kolumna warunek:");
          selector.add(warunek);
          JTextField toselect = new JTextField();
          selector.add(toselect);
          JButton wyszukaj= new JButton("WYSZUKAJ");
          selector.add(wyszukaj);
          wyszukaj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              table = (String) tablebox.getSelectedItem();
              where = toselect.getText();
              returned = dbc.Select(log, pas, table, where);
              if (returned == false) {
                selector.setVisible(false);
                JDialog error = new JDialog();
                error.setVisible(true);
                error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                error.setTitle("Bledne dane");
                JButton OK = new JButton("OK");
                error.setBounds(500, 300, 300, 150);
                error.add(OK);
                OK.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    error.setVisible(false);
                    setVisible(true);
                  }
                });
              } else {
                selector.setVisible(false);
                setVisible(true);

              }
            }
          });
      }
    });
    //worker action listener
    workbutt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        workaction = (String) workerbox.getSelectedItem();

        //wprowadzamy klienta
        if (workaction == "Wprowadz klienta") {
          setVisible(false);
          JDialog insert = new JDialog();
          insert.setTitle("Wprowadzanie klienta");
          insert.setDefaultCloseOperation(HIDE_ON_CLOSE);
          insert.setVisible(true);
          insert.setLayout(new GridLayout(5, 1));
          insert.setBounds(500, 300, 500, 350);
          JLabel neep = new JLabel("Wprowadz NIP");
          JTextField NIP = new JTextField(10);
          JLabel nazwa = new JLabel("Wprowadz nazwe firmy");
          JTextField firma = new JTextField(50);
          JButton x = new JButton("Wprowadz");
          insert.add(neep);
          insert.add(NIP);
          insert.add(nazwa);
          insert.add(firma);
          insert.add(x);
          x.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              NIPnumber = NIP.getText();
              name = firma.getText();
              returned = dbc.InsertK(log, pas, NIPnumber, name);
              insert.setVisible(false);
              if (returned == false) {
                JDialog error = new JDialog();
                error.setVisible(true);
                error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                error.setTitle("Bledne dane");
                JButton confirm = new JButton("OK");
                error.setBounds(500, 300, 300, 150);
                error.add(confirm);
                confirm.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    error.setVisible(false);
                    setVisible(true);
                  }
                });
              } else {
                setVisible(true);
              }
            }
          });
        }
        //wprowadzenie segregatora
        else if (workaction == "Wprowadz segregator"){
          setVisible(false);
          JDialog insert = new JDialog();
          insert.setTitle("Wprowadzanie segregatora");
          insert.setDefaultCloseOperation(HIDE_ON_CLOSE);
          insert.setVisible(true);
          insert.setLayout(new GridLayout(5, 1));
          insert.setBounds(500, 300, 500, 350);
          JLabel neep = new JLabel("Wprowadz ID_klienta");
          JTextField ID_kli = new JTextField(10);
          JLabel nazwa = new JLabel("Wprowadz nr.pokoju");
          JTextField nr_pok = new JTextField(50);
          JButton x = new JButton("Wprowadz");
          insert.add(neep);
          insert.add(ID_kli);
          insert.add(nazwa);
          insert.add(nr_pok);
          insert.add(x);
          x.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              returned = dbc.InsertS(log, pas, ID_kli.getText(), nr_pok.getText());
              insert.setVisible(false);
              if (returned == false) {
                JDialog error = new JDialog();
                error.setVisible(true);
                error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                error.setTitle("Bledne dane");
                JButton confirm = new JButton("OK");
                error.setBounds(500, 300, 300, 150);
                error.add(confirm);
                confirm.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    error.setVisible(false);
                    setVisible(true);
                  }
                });
              } else {
                setVisible(true);
              }
            }
          });
        }
       //wprowadzenie dokumentu
       else if(workaction=="Wprowadz dokument")
       {
         setVisible(false);
         JDialog insert= new JDialog();
         insert.setTitle("Wprowadzanie dokumentu");
         insert.setDefaultCloseOperation(HIDE_ON_CLOSE);
         insert.setVisible(true);
         insert.setLayout(new GridLayout(17,1));
         insert.setBounds(500,0,500,700);
         insert.add(new JLabel("Wprowadz ID_klienta:"));
         JTextField IDK = new JTextField();
         insert.add(IDK);
         insert.add(new JLabel("Wprowadz nazwe dokumentu:"));
         JTextField nazdok= new JTextField();
         insert.add(nazdok);
         insert.add(new JLabel("Wprowadz date przyjecia"));
         JTextField datprzyj = new JTextField();
         insert.add(datprzyj);
         insert.add(new JLabel("Wprowadz date dokumentu:"));
         JTextField datdok = new JTextField();
         insert.add(datdok);
         insert.add(new JLabel("Wprowadz ilosc stron:"));
         JTextField ilstr = new JTextField();
         insert.add(ilstr);
         insert.add(new JLabel("Wprowadz ID_segregatora:"));
         JTextField ID_seg = new JTextField();
         insert.add(ID_seg);
         insert.add(new JLabel("Wprowadz sygnature:"));
         JTextField sygn = new JTextField();
         insert.add(sygn);
         insert.add(new JLabel("Wprowadz ID_kategorii:"));
         JTextField ID_kat = new JTextField();
         insert.add(ID_kat);
         JButton dok= new JButton("WPROWADZ");
         insert.add(dok);
         dok.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
             insert.setVisible(false);
             String checker = IDK.getText();
             if (dbc.CheckK(log, pas, checker) == false) {
               System.out.println("JEEE");
               JDialog insert = new JDialog();
               insert.setTitle("Nowy klient. Wprowadz do bazy: ");
               insert.setDefaultCloseOperation(HIDE_ON_CLOSE);
               insert.setVisible(true);
               insert.setLayout(new GridLayout(5, 1));
               insert.setBounds(500, 300, 500, 350);
               JLabel neep = new JLabel("Wprowadz NIP");
               JTextField NIP = new JTextField(10);
               JLabel nazwa = new JLabel("Wprowadz nazwe firmy");
               JTextField firma = new JTextField(50);
               JButton x = new JButton("Wprowadz");
               insert.add(neep);
               insert.add(NIP);
               insert.add(nazwa);
               insert.add(firma);
               insert.add(x);
               x.addActionListener(new ActionListener() {
                 @Override
                 public void actionPerformed(ActionEvent e) {
                   NIPnumber = NIP.getText();
                   name = firma.getText();
                   returned = dbc.InsertK(log, pas, NIPnumber, name, checker);
                   insert.setVisible(false);
                   if (returned == false) {
                     JDialog error = new JDialog();
                     error.setVisible(true);
                     error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                     error.setTitle("Bledne dane");
                     JButton confirm = new JButton("OK");
                     error.setBounds(500, 300, 300, 150);
                     error.add(confirm);
                     confirm.addActionListener(new ActionListener() {
                       @Override
                       public void actionPerformed(ActionEvent e) {
                         error.setVisible(false);
                         insert.setVisible(true);
                       }
                     });
                   } else {
                     returned = dbc.InsertDok(log, pas, IDK.getText(), nazdok.getText(), datprzyj.getText(), datdok.getText()
                         , ilstr.getText(), ID_seg.getText(), sygn.getText(), ID_kat.getText());
                     if (returned == false) {
                       JDialog error = new JDialog();
                       error.setVisible(true);
                       error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                       error.setTitle("Bledne dane. Wprowadzenie nieudane");
                       JButton OK = new JButton("OK");
                       error.setBounds(500, 300, 300, 150);
                       error.add(OK);
                       OK.addActionListener(new ActionListener() {
                         @Override
                         public void actionPerformed(ActionEvent e) {
                           error.setVisible(false);
                           setVisible(true);
                         }
                       });
                     } else {
                       setVisible(true);
                     }
                   }
                 }
               });
             }
           }

         });


       }
       //wprowadzenie kategorii dokumentu
       else if(workaction=="Wprowadz kategorie dokumentu")
       {
         setVisible(false);
         JDialog insert= new JDialog();
         insert.setTitle("Wprowadzanie kategorii dokumentu");
         insert.setDefaultCloseOperation(HIDE_ON_CLOSE);
         insert.setVisible(true);
         insert.setLayout(new GridLayout(3,1));
         insert.setBounds(500,300,500,350);
         JLabel neep= new JLabel("Wprowadz kategorie dokumentu");
         JTextField NIP= new JTextField(10);
         JButton x= new JButton("Wprowadz");
         insert.add(neep);
         insert.add(NIP);
         insert.add(x);
         x.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
             NIPnumber=NIP.getText();
             returned=dbc.InsertKatdom(log,pas,NIPnumber);
             if(returned==false)
             {
               JDialog error = new JDialog();
               error.setVisible(true);
               error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
               error.setTitle("Bledne dane. Wprowadzenie nieudane");
               JButton OK = new JButton("OK");
               error.setBounds(500,300,300,150);
               error.add(OK);
               OK.addActionListener(new ActionListener() {
                 @Override
                 public void actionPerformed(ActionEvent e) {
                   error.setVisible(false);
                   setVisible(true);
                 }
               });
             }
             insert.setVisible(false);
           }
         });
       }
      }
    });

    //admin action listener
    adminbutt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        adminaction=(String) adminbox.getSelectedItem();

        //usuwanie rekordu
        if(adminaction=="Usun rekord")
        {
          setVisible(false);
          JDialog delete = new JDialog();
          delete.setDefaultCloseOperation(HIDE_ON_CLOSE);
          delete.setVisible(true);
          delete.setLayout(new GridLayout(5,1));
          delete.setBounds(500,300,500,350);
          JLabel usun= new JLabel("Z ktorej tabeli:");
          delete.add(usun);
          delete.add(tablebox);
          JLabel zczego = new JLabel ("Gdzie (kolumna warunek):");
          delete.add(zczego);
          JTextField todelete = new JTextField();
          delete.add(todelete);
          JButton deleteorder = new JButton ("DELETE");
          delete.add(deleteorder);
          deleteorder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              table= (String) tablebox.getSelectedItem();
              where= todelete.getText();
              returned=dbc.Delete(log,pas,table,where);
              if(returned==false)
              {
                delete.setVisible(false);
                JDialog error = new JDialog();
                error.setVisible(true);
                error.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                error.setTitle("Bledne dane");
                JButton OK = new JButton("OK");
                error.setBounds(500,300,300,150);
                error.add(OK);
                OK.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    error.setVisible(false);
                    setVisible(true);
                  }
                });
              }
              else {
                delete.setVisible(false);
                setVisible(true);

              }
            }
          });
        }
      }
    });
    rootbutt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        //tworzenie uzytkownikow
        {
          JDialog userdial= new JDialog();
          setVisible(false);
          userdial.setVisible(true);
          userdial.setTitle("Dodaj uzytkownika");
          userdial.setLayout(new GridLayout(7,1));
          userdial.setBounds(500,300,700,350);
          userdial.setDefaultCloseOperation(HIDE_ON_CLOSE);

          JLabel xx= new JLabel("Typ:");
          String [] s= {"Admin", "Pracownik", "Stazysta"};
          JComboBox c= new JComboBox(s);
          JLabel xxx= new JLabel("Login:");
          JTextField l= new JTextField();
          JLabel xxxx= new JLabel("Haslo:");
          JTextField p= new JTextField();
          JButton create= new JButton("CREATE");

          userdial.add(xx);
          userdial.add(c);
          userdial.add(xxx);
          userdial.add(l);
          userdial.add(xxxx);
          userdial.add(p);
          userdial.add(create);

          create.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              setVisible(true);
              userdial.setVisible(false);
              String type= (String) c.getSelectedItem();
              String login= l.getText();
              String password= p.getText();
              dbc.Createuser(log,pas,type,login,password);
            }
          });

        }
      }
    });

  }
}
